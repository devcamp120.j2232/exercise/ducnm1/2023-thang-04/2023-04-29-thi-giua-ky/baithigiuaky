package com.devcamp.baiThiGiuaKy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.baiThiGiuaKy.models.Classroom;

public interface ClassroomRepository extends JpaRepository<Classroom, Long> {
    public boolean existsByClassroomCode(String classroomCode);

    public Classroom findByClassroomName(String classroomName);

}
