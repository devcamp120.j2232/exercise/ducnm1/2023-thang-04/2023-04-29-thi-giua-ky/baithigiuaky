package com.devcamp.baiThiGiuaKy.models;

import lombok.*;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "classroom", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "classroom_code" }) })
@Data
public class Classroom {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "classroom_code", unique = true)
    @NotBlank(message = "Mã lớp học không được để trống")
    private String classroomCode;

    @Column(name = "classroom_name")
    @NotBlank(message = "Tên lớp học không được để trống")
    private String classroomName;

    @Column(name = "teacher_name")
    @NotBlank(message = "Tên giáo viên không được để trống")
    private String teacherName;

    @Column(name = "teacher_phone_number")
    @NotBlank(message = "Số điện thoại giáo viên không được để trống")
    @Size(min = 10, max = 10, message = "Số điện thoại phải bao gồm 10 ký tự")
    @Pattern(regexp = "^\\d{10}$", message = "Số điện thoại chỉ bao gồm chữ số")
    private String teacherPhoneNumber;

    @OneToMany(targetEntity = Student.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "classroom_id")
    private List<Student> students;

    @Column(name = "created_date")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(name = "updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

}
