package com.devcamp.baiThiGiuaKy.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "student")
@Data
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "student_code", unique = true)
    @NotBlank(message = "Mã học sinh không được để trống")
    private String studentCode;

    @Column(name = "student_name")
    @NotBlank(message = "Tên học sinh không được để trống")
    private String studentName;

    @Column(name = "gender")
    @NotBlank(message = "Giới tính không được để trống")
    private String gender;

    @Column(name = "birthday")
    @NotNull(message = "Ngày sinh không được để trống")
    private long birthday;

    @Column(name = "address")
    @NotBlank(message = "Địa chỉ không được để trống")
    private String address;

    @Column(name = "phone_number", unique = true)
    @NotBlank(message = "Số điện thoại không được để trống")
    @Size(min = 10, max = 10, message = "Số điện thoại phải bao gồm 10 ký tự")
    @Pattern(regexp = "^\\d{10}$", message = "Số điện thoại chỉ bao gồm chữ số")
    private String phoneNumber;

    @ManyToOne
    @JsonIgnore
    private Classroom classroom;

    @Transient
    private String classroomId;

    @Transient
    private String classroomName;

    @Column(name = "created_date")
    private long createdDate;

    @Column(name = "updated_date")
    private long updatedDate;

    public long getClassroomId() {
        return this.classroom.getId();
    }

    public String getClassroomName() {
        return this.classroom.getClassroomName();
    }

    // public String getBirthDayString() {
    // Date date = new Date(this.birthDay);
    // SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    // return df.format(date);
    // }
}
