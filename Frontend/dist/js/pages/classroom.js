import * as adminLteHelper from "../common/adminlte-utils.js";
import * as modalHelper from "../common/modal-utils.js";
import * as dataTableHelper from "../common/datatable-utils.js";
import * as errorHandler from "../common/errorHandler.js";

"use strict";
const ACTIVE_MENU_CONTENT = "Classroom";
const BASE_URL = "http://127.0.0.1:8080";
var gDATA_TABLE;
var gCLASSROOM_ENTITY

$(document).ready(function () {
    adminLteHelper.setActiveMenu(ACTIVE_MENU_CONTENT);
    adminLteHelper.setActiveBreadcrumbItemContent(ACTIVE_MENU_CONTENT);

    gCLASSROOM_ENTITY = new ClassroomEntity();

    addEventListener();
    initDatatable();
    loadAllClassrooms();
});

function initDatatable() {
    gDATA_TABLE = $("#classroom-table").DataTable({
        "columns": [
            {
                render: function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },
            { data: "classroomCode", defaultContent: "" },//set default content to avoid warning when data is null
            { data: "classroomName", defaultContent: "" },
            { data: "teacherName", defaultContent: "" },
            { data: "teacherPhoneNumber", defaultContent: "" },
            { data: "createdDate", defaultContent: "" },
            { data: "updatedDate", defaultContent: "" },
            {
                defaultContent:
                    `
                <i class="fa fa-edit action-edit" style="cursor:pointer;color:blue;margin-right:10px"></i>
                <i class="fa fa-trash action-delete" style="cursor:pointer;color:red"></i>
                `
            }
        ]
    });
}

function addEventListener() {
    $("#btn-show-modal-add").on("click", function () {
        showModalAddNew();
    });

    $("#btn-show-modal-delete-all").on("click", function () {
        showModalDeleteAll();
    });

    $("#modal-editor #btn-add").on("click", function () {
        gCLASSROOM_ENTITY.createNewClassroom();
    });

    $("#modal-editor #btn-edit").on("click", function () {
        gCLASSROOM_ENTITY.updateClassroom();
    });

    $("#modal-editor #btn-delete").on("click", function () {
        gCLASSROOM_ENTITY.deleteClassroom();
    });

    $("#modal-editor #btn-delete-all").on("click", function () {
        gCLASSROOM_ENTITY.deleteAllClassrooms();
    });

    $("#classroom-table").on("click", "tbody tr .action-edit", function () {
        var selectedRow = $(this).parents("tr");
        var selectedClassroom = gDATA_TABLE.row(selectedRow).data();
        console.log(selectedClassroom);
        gCLASSROOM_ENTITY.classroom = selectedClassroom;
        showModalEdit();
    });

    $("#classroom-table").on("click", "tbody tr .action-delete", function () {
        var selectedRow = $(this).parents("tr");
        var selectedClassroom = gDATA_TABLE.row(selectedRow).data();
        gCLASSROOM_ENTITY.classroom = selectedClassroom;
        showModalDelete();
    });
}

function loadAllClassrooms() {
    $.ajax({
        url: BASE_URL + "/classrooms",
        method: "GET",
        success: function (data) {
            dataTableHelper.setDatasource(gDATA_TABLE, data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
}

function showModalAddNew() {
    $("#modal-editor .modal-title").text("Thêm mới");
    $("#modal-editor button").hide();
    $("#modal-editor #btn-close").show();
    $("#modal-editor #btn-add").show();
    $("#modal-editor .row").show();
    $("#modal-editor .confirm-delete").hide();
    modalHelper.emptyInputFields("#modal-editor");

    $("#modal-editor").modal();
}

function showModalEdit() {
    $("#modal-editor .modal-title").text("Chi tiết");
    $("#modal-editor button").hide();
    $("#modal-editor #btn-close").show();
    $("#modal-editor #btn-edit").show();
    $("#modal-editor .row").show();
    $("#modal-editor .confirm-delete").hide();
    gCLASSROOM_ENTITY.fillDataToForm();

    $("#modal-editor").modal();
}

function showModalDelete() {
    $("#modal-editor .modal-title").text("Xóa");
    $("#modal-editor button").hide();
    $("#modal-editor #btn-close").show();
    $("#modal-editor #btn-delete").show();
    $("#modal-editor .row").hide();
    $("#modal-editor .confirm-delete").html(`<h6>Bạn có chắc chắn muốn xóa lớp ${gCLASSROOM_ENTITY.classroom.classroomName} không?</h6>`);
    $("#modal-editor .confirm-delete").show();

    $("#modal-editor").modal();
}

function showModalDeleteAll() {
    $("#modal-editor .modal-title").text("Xóa tất cả");
    $("#modal-editor button").hide();
    $("#modal-editor #btn-close").show();
    $("#modal-editor #btn-delete-all").show();
    $("#modal-editor .row").hide();
    $("#modal-editor .confirm-delete").html(`<h6>Bạn có chắc chắn muốn xóa tất cả các lớp không?</h6>`);
    $("#modal-editor .confirm-delete").show();

    $("#modal-editor").modal();
}

class Classroom {
    constructor(id, classroomCode, classroomName, teacherName, teacherPhoneNumber) {
        this.id = id;
        this.classroomCode = classroomCode;
        this.classroomName = classroomName;
        this.teacherName = teacherName;
        this.teacherPhoneNumber = teacherPhoneNumber;
    }
}

class ClassroomEntity {
    constructor() {
        this.classroom = new Classroom();
    }

    getData() {
        this.classroom.classroomCode = $("#modal-editor #classroom-code").val().trim();
        this.classroom.classroomName = $("#modal-editor #classroom-name").val().trim();
        this.classroom.teacherName = $("#modal-editor #teacher-name").val().trim();
        this.classroom.teacherPhoneNumber = $("#modal-editor #teacher-phone-number").val().trim();
    }

    fillDataToForm() {
        $.ajax({
            url: BASE_URL + "/classrooms/" + this.classroom.id,
            method: "GET",
            async: false,
            success: function (data) {
                this.classroom = data;
                $("#modal-editor #classroom-code").val(this.classroom.classroomCode);
                $("#modal-editor #classroom-name").val(this.classroom.classroomName);
                $("#modal-editor #teacher-name").val(this.classroom.teacherName);
                $("#modal-editor #teacher-phone-number").val(this.classroom.teacherPhoneNumber);
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    createNewClassroom() {
        this.getData();
        console.log(this.classroom);

        $.ajax({
            url: BASE_URL + "/classrooms/create",
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify(this.classroom),
            success: function (data) {
                alert("Tạo mới thành công");
                loadAllClassrooms();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    updateClassroom() {
        this.getData();
        $.ajax({
            url: BASE_URL + "/classrooms/update/" + this.classroom.id,
            method: "PUT",
            contentType: "application/json",
            data: JSON.stringify(this.classroom),
            success: function (data) {
                alert("Cập nhật thành công");
                loadAllClassrooms();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    deleteClassroom() {
        $.ajax({
            url: BASE_URL + "/classrooms/delete/" + this.classroom.id,
            method: "DELETE",
            success: function (data) {
                alert("Xóa thành công");
                loadAllClassrooms();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    deleteAllClassrooms() {
        $.ajax({
            url: BASE_URL + "/classrooms/delete",
            method: "DELETE",
            success: function (data) {
                alert("Xóa tất cả thành công");
                loadAllClassrooms();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }
}



