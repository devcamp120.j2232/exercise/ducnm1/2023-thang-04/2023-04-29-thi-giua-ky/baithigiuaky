export function setActiveMenu(menuContent) {
    $('.nav-link').filter(function () {
        return $(this).find('p').text() === menuContent;
    }).addClass("active");
}

export function setActiveBreadcrumbItemContent(breadcrumbContent) {
    const activeBreadcrumbItem = document.querySelector("li.breadcrumb-item.active");
    if (activeBreadcrumbItem) {
        activeBreadcrumbItem.textContent = breadcrumbContent;
    } else {
        console.error("No active breadcrumb item found.");
    }
}